AHDL -- asynchronous hardware description language
==================================================
(preliminary draft)

Asynchronous does notmean here that it would describe only async logical systems. Instead, it means that it can define also asynchronous systems.

Key feature of the language is that entities are evaluated in non-deterministic order, more clearly: validation calculates all possible evaluation orders. They need to result a valid output.

As all programming (and hardware description) languages, key thing is EVALUATION.

Entities of the languages are these:

1. Inputs. They are called I0 ... their value can be 0 or 1. An input entity has 0 to infinite output entities, but no inputs.

2. Outputs. They are called O0... their value can be also 0 or 1. An output entity has exactly 1 input entities and no outputs.

3. Gate. These are the two-input logical functions, they are called G0...G15. Many of them are meaningless (for example, G0 is always constant zero), these are still valid.

A Gate entity has exactly 2 input entities and 1 to infinite output entities.

4. Bit. A bit represents a single-bit internal state of the logical system. A bit has two input entities, they are called W(rite) and I(nput). If W is 1, then bit changes to the value of I. A bit can also have 1 to infinite output entities, they see its current internal state in their input.

5. F function. F determines for all possible Inputs and States their Output and next State:

$$F \in (I,S) \rightarrow (O,S')$$

In validation, F is always calculated until State does not change any more without an input change:

$$F(I,S)=(O,S)$$

(Note the lack of apostrophe in the second S).

Having here an infinite loop or clearly too long evaluation (to be determined, exactly how long) means invalid hardware description.
---

VALIDATION is the process determining that all possible input transition with all possible internal states results F. Input transitions happen bit-by-bit, one bit at once. Also state transitions happen one bit at once.

Very clearly, we have here a very quickly growing hardness in the validation. For example, a 32 bit incrementer would have 32 input pins, 0 state and 32 output (forget overflow, that is a PoC).

That means that we have 32+32=64 possible transitions, all of them happen in in indeterminted order, resulting 32! possible transitions. We need to calculate 2^32*2^32*32, which is $\approx 3e+82$. That clearly does not work.

What works: instead of creating a single large logical system, we create a network of small asynchronous logical systems. These systems already can be validated.

Then, some magic happens (TO BE DONE) and somehow we prove that this network of small sub-systems also results a valid system.

For example, the complexity of a 2-bit incrementer is only 4*4*2!=32 which is trivial. More clearly... not exactly, we need to have some synchronization pins, too.

Idea:

Input pins: I0,I1,DOIT
Output pins: O0,O1,DONE

Level change of "DOIT" triggers the system to start the calculation. If it is done, it changes the level of DONE. After that, no more change of the O0, O1 output is allowed to happen without an input change. Of course, DONE can serve as the DOIT input for other sub-systems.

Here we only have 3 inputs, that is 8*8*3! = 384 transitions to validate, thus it can be done well.

My initial tests show that about 8 bit (input + state) is the limit where validation can yet happen "easily" on a desktop machine with a naive brute algorithm.
