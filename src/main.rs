use std::io;
use std::io::BufRead;

fn main() -> io::Result<()> {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();

  while let Some(line) = lines.next() {
    match line {
      Ok(s) => println!("{}", s),
      Err(e) => println!("{}", e)
    }
  }

  Ok(())
}
